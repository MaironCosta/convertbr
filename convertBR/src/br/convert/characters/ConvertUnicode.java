package br.convert.characters;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * This final class work together with your mother class 
 * and is used to convert text to unicode character.<br>
 * 
 * @author Mairon Costa
 * @since 03-03-2014
 * @version 1.1 - 09-10-2014
 * 
 * */
@SuppressWarnings({"unused"})
public final class ConvertUnicode extends ConverterCharacter {

	public ConvertUnicode() {
		// TODO Auto-generated constructor stub
	}

	public String unconvertSpecialCharacter(String text) throws IllegalArgumentException {
		// TODO Auto-generated method stub
		
		if (text == null)
			throw new IllegalArgumentException("text is null");

		Map<String, String> characters = this.getAllCharacters();
		
		StringBuilder sb = new StringBuilder(text);
		
		for (Map.Entry<String, String> character : characters.entrySet())
			 sb = new StringBuilder(sb.toString().replace(character.getKey(), character.getValue()));
		
		return sb.toString();
		
	}

	public String convertToCode(char letter) {
		
	    String hexa = Integer.toHexString((int)letter);

	    String prefix;
	    if( hexa.length() == 1 ) {
	        prefix = "\\u000";
	    } else if( hexa.length() == 2 ) {
	        prefix = "\\u00";
	    } else if( hexa.length() == 3 ) {
	        prefix = "\\u0";
	    } else {
	        prefix = "\\u";
	    }
	    
	    return prefix + hexa;
	}
	
	@Override
	public String convertToCode(String text) throws IllegalArgumentException {
		// TODO Auto-generated method stub

		if (text == null)
			throw new IllegalArgumentException("text is null");

		Map<String, String> charac = super.getAllCharacters();
		
		StringBuilder sb = new StringBuilder(text);
		
		for (Map.Entry<String, String> character : charac.entrySet())
			 sb = new StringBuilder(sb.toString().replace(character.getValue(), character.getKey()));
		
		return sb.toString();
	}

	private void autoCreateCaracteresEspeciais() {

		StringBuilder sb = new StringBuilder("áÁàÀâÂãÃéÉèÈêÊíÍìÌóÓòÒõÕôÔúÚùÙüÜ");
		
		char[] s = sb.toString().toCharArray();
		for (int i = 0; i <= s.length - 1; i++) {
			
			try {
				
				System.out.println(this.convertToCode(s[i]) + " , " + String.valueOf(s[i]));

			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("ERRO: " + e.getMessage());
				continue;
			}

		}
		
	}
		
	
	/*
	characters.put("\u00e1", "Ã¡");
	characters.put("\u00c1", "Ã?");
	characters.put("\u00e0", "Ã ");
	characters.put("\u00c0", "Ã€");
	characters.put("\u00e2", "Ã¢");
	characters.put("\u00e2", "Ã¢");
	characters.put("\u00c2", "Ã‚");
	characters.put("\u00e3", "Ã£");
	characters.put("\u00c3", "Ãƒ");
	characters.put("\u00e9", "Ã©");
	characters.put("\u00c9", "Ã‰");
	characters.put("\u00e8", "Ã¨");
	characters.put("\u00c8", "Ãˆ");
	characters.put("\u00ea", "Ãª");
	characters.put("\u00ca", "ÃŠ");
	characters.put("\u00ed", "Ã­");
	characters.put("\u00cd", "Ã?");
	characters.put("\u00ec", "Ã¬");
	characters.put("\u00cc", "ÃŒ");
	characters.put("\u00f3", "Ã³");
	characters.put("\u00d3", "Ã“");
	characters.put("\u00f2", "Ã²");
	characters.put("\u00d2", "Ã’");
	characters.put("\u00f5", "Ãµ");
	characters.put("\u00d5", "Ã•");
	characters.put("\u00f4", "Ã´");
	characters.put("\u00d4", "Ã”");
	characters.put("\u00fa", "Ãº");
	characters.put("\u00da", "Ãš");
	characters.put("\u00f9", "Ã¹");
	characters.put("\u00d9", "Ã™");
	characters.put("\u00fc", "Ã¼");
	characters.put("\u00dc", "Ãœ");
	*/
}
