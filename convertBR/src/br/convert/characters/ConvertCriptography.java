package br.convert.characters;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class ConvertCriptography {

	public enum CriptographyType {

		MD5 ("MD5");
		
		private String criptographyType;

		public String getCriptographyType() {
			return criptographyType;
		}
		
		private CriptographyType(String criptographyType) {
			// TODO Auto-generated constructor stub
			this.criptographyType = criptographyType;
		}
		
	}
	
	public ConvertCriptography() {
		// TODO Auto-generated constructor stub
	}
	
	public static String criptography (CriptographyType criptographyType, String input) throws IllegalArgumentException{		

		if (input == null || "".equals(input)) 
			throw new IllegalArgumentException("input is null or is a empty string");
		
		String cript = null;
		
		try {
			
			MessageDigest digest = MessageDigest.getInstance(criptographyType.getCriptographyType());
			digest.update(input.getBytes(), 0, input.length());
			
			cript = new BigInteger(1, digest.digest()).toString(16);
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return cript;
	}

}
