package br.convert.characters;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

import br.convert.utils.Locales;

public final class ConvertMoney {	

	public ConvertMoney() {
		// TODO Auto-generated constructor stub
	}	
	
	public static String moneyLocale (BigDecimal valor, Locale locale) throws IllegalArgumentException{		

		if (valor == null) 
			throw new IllegalArgumentException("valor is null");

		if (locale == null)
			throw new IllegalArgumentException("locale is null");
		
		NumberFormat nf = NumberFormat.getCurrencyInstance(locale);
		nf.setMinimumFractionDigits(2);
		nf.setCurrency(Currency.getInstance(locale));
		
		return nf.format(valor);
		
	}
	
	public static void main(String[] args) {
		
		System.out.println(ConvertMoney.moneyLocale(new BigDecimal(10), Locales.locale_EN_USA));
		
	}
		
}

