package br.convert.file;


@SuppressWarnings("rawtypes")
public abstract class Conversor<T> implements IConversor<T>  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public enum TYPE {
		
		XML (new ConversorXML<>());
		
		private Conversor conversor;
		
		private TYPE (Conversor conversor) {
			this.conversor = conversor;
		}
		
		public Conversor getConversor() {
			return this.conversor;
		}
		
	};
		
	/**
	 * This method must receive the type of file to convert
	 * 
	 * */
	public final static Conversor getInstance(TYPE type) {
		return type.getConversor();
	}
	
}
