package br.convert.file;

import java.io.IOException;
import java.io.Reader;

import javax.xml.bind.JAXBException;


/**
 * This class is a XML handler. 
 * */
public interface IConversorXML<T> extends IConversor<T> {

	/**
	 * This method convert to a XML format. <br/>
	 * @param Class<T> clazz - receive any class.<br/>
	 * @param Reader reader - must contain a XML string format in same type of clazz.<br/>
	 * @exception 	  
	 * */
	public T read (Class<T> clazz, Reader reader) throws IOException, JAXBException;

	/**
	 * This method return a string in XML format.
	 * */
	public String parseToString (T t) throws JAXBException;
	
} 
