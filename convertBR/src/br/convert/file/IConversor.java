package br.convert.file;

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;

/**
 * This class is a file handler. 
 * */
public interface IConversor<T> extends Serializable {	
	
	/**
	 * This method convert to an object.
	 * 
	 * @param Class<T> clazz - receive any class.
	 * @param Reader reader - must contain a file string format in same type of clazz.
	 * 
	 * @exception 
	 * */
	public T read (Class<T> clazz, Reader reader) throws IOException, Exception;
	
	/**
	 * This method parse a object to a string to be handler.
	 * @exception 
	 * */
	public String parseToString (T t) throws Exception;
	
}
