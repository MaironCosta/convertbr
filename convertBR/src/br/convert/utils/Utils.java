package br.convert.utils;

import java.util.Random;

public class Utils {
	
	public static void random (char[] letras) {
		
		Random random = new Random();  
		
		StringBuffer sb = new StringBuffer(); 
		byte quantidadeLetras = 8;
		byte count = 0;
		
		do {
			
			int index = random.nextInt(letras.length);  
			sb.append(letras[index]);
			
			count++;
			
		} while (count < quantidadeLetras);
		
		System.out.println("length: " + sb.length() + " senha: " + sb);
		
	}
		
	public static void main(String[] args) {
		
		StringBuilder letras = new StringBuilder("ABCDEFGHIJKLMNOPQRSTUVYWXZ");  
		letras.append(letras.toString().toLowerCase());
		
		for (byte i = 0; i <=9 ; i++) letras.append(i);
		
		random(letras.toString().toCharArray());
		
	}
	
}
