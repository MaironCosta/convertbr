package br.convert.utils;

import java.util.Locale;

public interface Locales {
	
	Locale locale_PT_BR = new Locale("pt","BR"); // PT BRASIL
	Locale locale_FR_FR = new Locale("fr","FR"); // FRANC�S - FRN�A
	Locale locale_ES_ES = new Locale("es","ES"); // ESPANHOL - ESPANHA
	Locale locale_EN_USA = new Locale("en", "US");	// INGL�S USA
	Locale locale_EN_GB = new Locale("en","GB");	// Reino Unido ingl�s
	
	Locale localeDefault = locale_PT_BR;
	
}
