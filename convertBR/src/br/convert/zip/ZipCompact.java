package br.convert.zip;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipCompact extends ZIPHandle {

	ZipCompact() {
		// TODO Auto-generated constructor stub
	}
		
	public void add(String fileName, ByteArrayOutputStream file) throws ZipHandleException {
		
		this.validar(fileName, file);		
		super.getByteArrayOSFile().put(fileName, file);
		
	}
	
	private void validar(String fileName, OutputStream f) throws ZipHandleException {
		// TODO Auto-generated method stub
		
		List<String> erros = new ArrayList<String>();
		
		if (getValidacoes().isEmpty(fileName))
			erros.add("File name is empty.");
		
		if (f == null)
			erros.add("File not exists.");		

		for (Map.Entry<String, ByteArrayOutputStream> file : super.getByteArrayOSFile().entrySet()) {

			if (file.getKey().equals(fileName)) {
				erros.add("File name already exists.");
				break;
			}
			
		}		
		
		if (!getValidacoes().isEmpty(erros))
			throw new ZipHandleException(erros);
		
	}

	public ByteArrayOutputStream getByteArrayOutputStream () throws IOException {
		
		ByteArrayOutputStream arquivoSaida = new ByteArrayOutputStream();
		ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(arquivoSaida));
		
		for (Map.Entry<String, ByteArrayOutputStream> file : super.getByteArrayOSFile().entrySet()) {

			ZipEntry entry = new ZipEntry(file.getKey());
			out.putNextEntry(entry);

			out.write(file.getValue().toByteArray());
			
		}
		
		out.close();
		
		return arquivoSaida;
	}
	
	public void compactFromDirectoryToDirectory (String outDirectory, String fileName, String directoryFile) throws FileNotFoundException, IOException {

		final int BUFFER = 2048;

		BufferedInputStream origin = null;

		//			dest = new FileOutputStream("c:\\zip\\zipFiles.zip");
//		outDirectory = "c:\\zip\\";
//		fileName = "zipFile";
		FileOutputStream dest = new FileOutputStream(outDirectory + fileName + ".zip");

		ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));
		out.setMethod(ZipOutputStream.DEFLATED);
		byte data[] = new byte[BUFFER];

		// get a list of files from current directory
//		entryDirectory = "c:\\file\\";
		File f = new File(directoryFile);
		String files[] = f.list();

		for (int i=0; i < files.length; i++) {

//			System.out.println("Adding: "+ directoryFile + files[i]);
			FileInputStream fi = new FileInputStream(directoryFile + files[i]);
			origin = new BufferedInputStream(fi, BUFFER);
			ZipEntry entry = new ZipEntry(files[i]);
			out.putNextEntry(entry);
			int count;
			while((count = origin.read(data, 0, BUFFER)) != -1) {
				out.write(data, 0, count);
			}

			origin.close();

		}

		out.close();

	}
	
	public void compactToDirectory (String outDirectory, String fileName) throws FileNotFoundException {
		
		FileOutputStream dest = new FileOutputStream(outDirectory + fileName + ".zip");
		ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));
		
	}
	
	public static void main(String[] args) {
		
		try {
			
			ZipCompact zipCompact = (ZipCompact) ZIPHandle.getInstance(ZipType.COMPACT);
			zipCompact.compactFromDirectoryToDirectory(null, null, null);
		
		} catch (ZipHandleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
