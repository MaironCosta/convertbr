package br.convert.zip;

import java.util.ArrayList;
import java.util.List;

public class ZipHandleException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<String> erros = new ArrayList<String>(1);	

	public ZipHandleException(List<String> erros) {
		// TODO Auto-generated constructor stub
		this.erros = erros;
	}

	public ZipHandleException(String erro) {
		super();
		this.erros.add(erro);
	}

	public List<String> getErros() {
		return erros;
	}	

}
