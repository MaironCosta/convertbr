package br.convert.zip;

public enum ZipType {

	COMPACT(new ZipCompact());
	
	public ZIPHandle zipHandle;
	
	private ZipType (ZIPHandle zipHandle) {
		this.zipHandle = zipHandle;
	}
	
	public ZIPHandle getZIPHandle () {
		return this.zipHandle;
	}
	
}
