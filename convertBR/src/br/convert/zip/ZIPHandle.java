package br.convert.zip;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public abstract class ZIPHandle {
	
	private Map<String, ByteArrayOutputStream> byteArrayOSFile = new HashMap<String, ByteArrayOutputStream>();
	
	private static Validacoes validacoes;

	public static ZIPHandle getInstance (ZipType zipType) throws ZipHandleException {

		if (zipType == null)
			throw new ZipHandleException("zipType is null");
		
		return zipType.getZIPHandle();
	}

	Map<String, ByteArrayOutputStream> getByteArrayOSFile() {
		return byteArrayOSFile;
	}
	
	Validacoes getValidacoes () {
		
		if (validacoes == null)
			validacoes =  new Validacoes();
		
		return validacoes;
	}
	
	class Validacoes {

		public boolean isEmpty (String string) {
			
			if (string == null)
				return true;
			if (string.trim().isEmpty())
				return true;
			
			return false;
		}

		public boolean isEmpty (Collection<?> list) {

			if (list == null)
				return true;
			if (list.isEmpty())
				return true;

			return false;
		}

	}

	public static void main(String[] args) {
		
		ZipCompact zipCompact;
		
		try {
			
			zipCompact = (ZipCompact) ZIPHandle.getInstance(ZipType.COMPACT);
			zipCompact.add("teste", new ByteArrayOutputStream());
			
			zipCompact.getByteArrayOutputStream();
			
		} catch (ZipHandleException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
